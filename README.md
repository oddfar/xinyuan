## 简介



**一个简洁美观，功能简单并且自适应的java社团纳新管理小网站**

前端使用Bootstrap。支持移动端自适应

项目简单，适用于学完springboot的简单练手项目。

### 主要内容

- 在线提交个人简历

- 用户分权

  暂时两个权限：admin系统管理员 和 user成员

- 录取后自动添加账号

  在面试页面中，点击录取后自动添加，账号为手机号，密码123456的成员账号

- 登录分权跳转

  login登录页面后，根据用户权限跳转至admin或user页面

- ........

### 技术栈

不想重复造轮子，前端模块则在网上“偷”的现成的：

- Bootstrap
- Layui
- layer弹出层

后端技术栈：

- Springboot 2.1.0
- Spring-boot--security
- Lombok
- MySQL、Mybatis
- Thymeleaf



## sql表

新建名为“xinyuna”数据库

### 配置信息表

- config

  配置信息表，仅两个配置数据

| name     | value     | remark                                      |
| -------- | --------- | ------------------------------------------- |
| online   | false     | 是否开启线上提交简历（true开启，false关闭） |
| qq_group | 782212497 | 纳新通知QQ群                                |

### 登录表

三个表构建一个简单的区分权限的登录

- user

用户列表，存放账号和密码

| id   | username | password |
| ---- | -------- | -------- |
| 1    | root     | 88888888 |

- role

权限信息表，仅有两权限（可自己更改项目架构添加权限）

| id   | name        | name_zh    |
| ---- | ----------- | ---------- |
| 1    | ROLE_admin  | 系统管理员 |
| 2    | ROLE_member | 成员       |

- user_role

分配用户权限的表，默认给 root 分配  ROLE_admin 权限

| id   | uid  | rid  |
| ---- | ---- | ---- |
| 1    | 1    | 1    |

### 用户表

面试表：interview

成员表：member

## 目录简介

java文件目录树

```html
│  Application.java						- 运行
│
├─config
│      SecurityConfig.java				- 登录路由配置
│
├─controller	
│      InterviewController.java			- 面试者相关路由控制
│      MemberController.java			- 成员者相关路由控制
│      RouterController.java			- 基本路由控制
│
├─mapper								- mapper接口类
│      ConfigMapper.java				- 配置
│      InterviewMapper.java				- 面试
│      MemberMapper.java				- 成员
│      UserMapper.java					- 登录用户
│
├─pojo
│      Config.java						- 配置类
│      Interview.java
│      Member.java
│      Role.java						- 权限类
│      User.java						- 登录用户类
│
└─service								- 用户登录服务层
        UserService.java
```

资源目录树：

```html
│  application.yml						- 配置文件
│
├─mybatis
│  └─mapper								- sql中xml文件
│
├─static
│
└─templates		
    │  doc.html							- doc页面
    │  index.html
    │  login.html
    │  register.html					- 在线提交个人简历
    │
    ├─admin								- admin后台管理模块
    │      add.html
    │      interviewList.html
    │      main.html
    │      update.html
    │
    ├─commons							- 公共模块
    │      commons.html
    │      front.html
    │
    ├─error								- 错误模块
    │      403.html
    │      404.html
    │
    └─user								- user用户模块
            member.html
```





## 项目上手

- 导入数据库“xinyuan.sql”

- 修改`application.yml`下数据库名称和密码
- idea下打开



**项目部署**

切换到jar包目录，后台运行

```sh
nohup java -jar xinfar-1.1.jar >logs.txt &
```

命令运行成功后，会返回一个进程号，可以通过 `kill -9` 命令杀死这个进程来直接关闭。

如果忘了进程号，可以通过指令来查看当前运行的jar包程序进程号

```sh
ps -ef|grep xxx.jar
```



项目占用端口号：9001

可通过nginx反向代理

