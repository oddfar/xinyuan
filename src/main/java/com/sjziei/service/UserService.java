package com.sjziei.service;

import com.sjziei.mapper.UserMapper;
import com.sjziei.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author zhiyuan
 * @date 2021/4/17 16:10
 */
@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserMapper userMapper;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {


        //查询用户
        User user = userMapper.getUserByName(s);

        if (user == null) {
            throw new UsernameNotFoundException("用户不存在！");
        }else{
            //查询并设置用户权限
            user.setRoles(userMapper.getUserRolesById(user.getId()));

            //查询数据库密码并转化
            String pwd = new BCryptPasswordEncoder().encode(user.getPassword());
            user.setPassword(pwd);

            //输出登录用户
            System.out.println(user);


            //返回user
            return user;
        }



    }
}




