package com.sjziei.mapper;

import com.sjziei.pojo.Member;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zhiyuan
 * @date 2021/4/11 12:39
 */
@Mapper
@Repository
public interface MemberMapper {

    int addUser(Member member);

    int deleteUser(@Param("id") Integer id);

    int updateUser(Member member);

    /**
     * 查询全部用户
     * @return
     */
    List<Member> queryUserList();

    /**
     * 通过id查询用户
     * @param id
     * @return
     */
    Member queryUserById(@Param("id") Integer id);

    /**
     * 通过手机号查询用户
     * @param phone
     * @return
     */
    Member queryUserByPhone(Long phone);
}
