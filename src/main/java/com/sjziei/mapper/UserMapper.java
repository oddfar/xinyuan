package com.sjziei.mapper;


import com.sjziei.pojo.Role;
import com.sjziei.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author zhiyuan
 * @date 2021/4/17 13:37
 */
@Mapper
@Repository
public interface UserMapper {
    /**
     * 通过登录用户名获取用户信息
     *
     * @param username
     * @return
     */
    User getUserByName(String username);

    /**
     * 获取指定id用户的权限
     *
     * @param id
     * @return
     */
    List<Role> getUserRolesById(Integer id);


    /**
     * 增加一个后台用户
     * @param user 用户
     * @return
     */
    int addUser(User user);

    /**
     * 增加一个用户权限
     * @param map uid rid
     *             rid为1管理员，2普通成员
     * @return
     */
    int addUserRole(Map<String, Integer> map);

//    int delUserByPhone(String phone);

    int delUserById(Integer id);

    int delUserRoleByUid(Integer uid);

}
