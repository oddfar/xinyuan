package com.sjziei.mapper;

import com.sjziei.pojo.Interview;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zhiyuan
 * @date 2021/4/11 12:39
 */
@Mapper
@Repository
public interface InterviewMapper {

    int addUser(Interview interview);

    int deleteUser(@Param("id") Integer id);


    int updateUser(Interview interview);


    /**
     * 查询全部用户
     * @return
     */
    List<Interview> queryUserList();

    /**
     * 通过id查询用户
     * @param id
     * @return
     */
    Interview queryUserById(@Param("id") Integer id);

    /**
     * 通过电话查询用户
     * @param telephone
     * @return
     */
    Interview queryUserByPhone(Long telephone);

}
