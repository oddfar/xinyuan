package com.sjziei.mapper;

import com.sjziei.pojo.Config;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhiyuan
 * @date 2021/4/17 23:52
 */
@Mapper
public interface ConfigMapper {
    List<Config> getAll();

}
