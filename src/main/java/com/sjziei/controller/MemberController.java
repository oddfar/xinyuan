package com.sjziei.controller;

import com.sjziei.mapper.MemberMapper;
import com.sjziei.mapper.UserMapper;
import com.sjziei.pojo.Member;
import com.sjziei.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhiyuan
 * @date 2021/4/11 18:08
 */
@Controller
public class MemberController {
    @Autowired
    MemberMapper memberMapper;
    @Autowired
    UserMapper userMapper;

    //前往管理页面首页
    @RequestMapping({"/admin", "/admin/m/list"})
    public String toLogin(HttpSession session, Model model) {


        //获取登录的用户名
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        //传给前端
        session.setAttribute("loginUserName", username);

        List<Member> members = memberMapper.queryUserList();
        //传递给前端member
        model.addAttribute("members", members);

        //操作页面是：member，前端用来区分
        model.addAttribute("operation", "member");

        return "admin/main";
    }

    @RequestMapping("/admin/m/update/{id}")
    public String InterviewUpdate(@PathVariable("id") Integer id, Model model) {

        Member member = memberMapper.queryUserById(id);
        model.addAttribute("interview", member);
        model.addAttribute("operation", "member");
        return "admin/update";
    }

    @RequestMapping("/admin/m/toUpdate")
    public String InterviewToUpdate(Member member) {
        int i = memberMapper.updateUser(member);
        return "redirect:/admin/m/list";
    }

    @RequestMapping("/admin/m/add")
    public String InterviewToAdd(Model model) {
        model.addAttribute("operation", "member");
        return "admin/add";
    }

    @RequestMapping("/admin/m/toAdd")
    public String InterviewToAdd(Member member) {
        int i = memberMapper.addUser(member);

        //添加后台成员账号

        User user = new User();
        user.setUsername(member.getTelephone().toString());
        user.setPassword("123456");
        userMapper.addUser(user);

        //添加权限
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("uid", user.getId());
        map.put("rid", 2);
        userMapper.addUserRole(map);

        return "redirect:/admin/m/list";
    }

    @RequestMapping("/admin/m/del/{id}")
    public String InterviewToDelete(@PathVariable("id") Integer id) {
        Member member = memberMapper.queryUserById(id);
        //删除
        int i = memberMapper.deleteUser(id);

        //查询user
        User user = userMapper.getUserByName(member.getTelephone().toString());

        //删除user表数据
        userMapper.delUserById(user.getId());
        //删除user_role数据
        userMapper.delUserRoleByUid(user.getId());


        return "redirect:/admin/m/list";
    }

    //用户跳转
    @RequestMapping("/user")
    public String user(HttpSession session) {
        //获取用户登录名，即手机号
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        //获取用户成员信息
        Member member = memberMapper.queryUserByPhone(Long.parseLong(username));

        System.out.println(member);
        //设置session
        session.setAttribute("member", member);

        return "user/member";
    }


    //修改资料
    @RequestMapping("/user/update")
    public String userUpdate(Member member,HttpSession session) {
        //获取登录的session
        Member m = (Member) session.getAttribute("member");

        member.setId(m.getId());
        member.setTelephone(m.getTelephone());

        memberMapper.updateUser(member);

        //更新session
        session.setAttribute("member", member);

        return "user/member";
    }
}
