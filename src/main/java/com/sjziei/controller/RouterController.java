package com.sjziei.controller;

import com.sjziei.mapper.ConfigMapper;
import com.sjziei.mapper.InterviewMapper;
import com.sjziei.pojo.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.List;

/**
 * @author zhiyuan
 * @date 2021/4/11 14:06
 */
@Controller
public class RouterController {
    @Autowired
    InterviewMapper interviewMapper;
    @Autowired
    ConfigMapper configMapper;


    @RequestMapping({"/", "/index","/index.html"})
    public String index() {
        return "index";
    }


    @RequestMapping("/login")
    public String login() {
        //用户区分，user转向user，管理转向admin
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority authority : authorities) {
            if(authority.getAuthority().contains("ROLE_admin")){
                return "redirect:/admin";
            }
            if(authority.getAuthority().contains("ROLE_member")){
                return "redirect:/user";
            }

        }
        return "login";
    }

    @RequestMapping("/loginSuccess")
    public String loginSuccess(){
        //用户区分，user转向user，管理转向admin
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority authority : authorities) {
            if(authority.getAuthority().contains("ROLE_admin")){
                return "redirect:/admin";
            }
            if(authority.getAuthority().contains("ROLE_member")){
                return "redirect:/user";
            }

        }
        return "login";

    }
    @RequestMapping("/register")
    public String register(HttpSession session){
        //取出数据库
        List<Config> all = configMapper.getAll();
        for (Config config : all) {
            if("qq_group".equals(config.getName())){
                session.setAttribute("qq_group",config.getValue());
            }
            if("online".equals(config.getName())){
                session.setAttribute("online",config.getValue());
                //判断是否开启线上提交简历
                if ("false".equals(config.getValue())){
                    session.setAttribute("msg","暂未开启线上提交个人简历，请注意群通知");
                }else{
                    session.removeAttribute("msg");
                }
            }

        }


        return "register";
    }



}
