package com.sjziei.controller;

import com.sjziei.mapper.InterviewMapper;
import com.sjziei.mapper.MemberMapper;
import com.sjziei.mapper.UserMapper;
import com.sjziei.pojo.Interview;
import com.sjziei.pojo.Member;
import com.sjziei.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhiyuan
 * @date 2021/4/11 13:38
 */
@Controller
public class InterviewController {
    @Autowired
    InterviewMapper interviewMapper;
    @Autowired
    MemberMapper memberMapper;
    @Autowired
    UserMapper userMapper;


    //查询面试人员列表
    @RequestMapping("/admin/i/list")
    public String InterviewList( Model model) {

        List<Interview> interviews = interviewMapper.queryUserList();
        model.addAttribute("interviews", interviews);

        return "admin/interviewList";

    }

    @RequestMapping("/admin/i/update/{id}")
    public String InterviewUpdate(@PathVariable("id") Integer id, Model model) {

        Interview interview = interviewMapper.queryUserById(id);
        if(interview==null){
            return "redirect:/admin/i/list";
        }
        model.addAttribute("interview", interview);
        //操作页面是：interview，前端用来区分
        model.addAttribute("operation","interview");
        return "admin/update";
    }
    @RequestMapping("/admin/i/toUpdate")
    public String InterviewToUpdate(Interview interview){
        int i = interviewMapper.updateUser(interview);
        return "redirect:/admin/i/list";
    }
    @RequestMapping("/admin/i/add/")
    public String InterviewToAdd(Model model) {
        model.addAttribute("operation","interview");
        return "admin/add";
    }
    @RequestMapping("/admin/i/toAdd")
    public String InterviewToAdd(Interview interview){

        int i = interviewMapper.addUser(interview);

        return "redirect:/admin/i/list";
    }

    //录取操作
    @RequestMapping("/admin/i/admission/{id}")
    public String InterviewToAdmission(@PathVariable("id") Integer id){

        Interview interview = interviewMapper.queryUserById(id);
        if(interview==null){
            return "redirect:/admin/i/list";
        }
        Member member = new Member();
        member.setName(interview.getName());
        member.setDepartment_class(interview.getDepartment_class());
        member.setSex(interview.getSex());
        member.setTelephone(interview.getTelephone());
        member.setQq(interview.getQq());

        //添加成员
        int i = memberMapper.addUser(member);

        //添加后台成员账号

        User user = new User();
        user.setUsername(member.getTelephone().toString());
        user.setPassword("123456");
        userMapper.addUser(user);

        //添加权限
        Map<String,Integer> map = new HashMap<String,Integer>();
        map.put("uid",user.getId());
        map.put("rid",2);
        userMapper.addUserRole(map);

        //删除面试
        interviewMapper.deleteUser(id);




        return "redirect:/admin/i/list";
    }


    // 在线提交个人简历
    @PostMapping("/i/toAdd")
    public String InterviewAdd(Interview interview, HttpSession session){
        String online = (String) session.getAttribute("online");
        //是否开启在线提交
        if ("true".equals(online)){
            Interview i = interviewMapper.queryUserByPhone(interview.getTelephone());
            if(i==null){
                //添加
                interviewMapper.addUser(interview);
                session.setAttribute("msg","提交成功");
            }else{
                //更新
                interviewMapper.updateUser(interview);
                session.setAttribute("msg","更新成功");
            }
        }


        return "redirect:/register";
    }

    @RequestMapping("/admin/i/del/{id}")
    public String InterviewToDelete(@PathVariable("id") Integer id) {
        int i = interviewMapper.deleteUser(id);
        return "redirect:/admin/i/list";
    }



}
