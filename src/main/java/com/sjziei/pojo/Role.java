package com.sjziei.pojo;

import lombok.Data;

/**
 * @author zhiyuan
 * @date 2021/4/17 16:06
 */
@Data
public class Role {
    private Integer id;
    private String name;
    private String name_zh;
}
