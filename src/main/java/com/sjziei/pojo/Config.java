package com.sjziei.pojo;

import lombok.Data;

/**
 * 配置类，设置纳新通知群，及是否开启线上提交资料
 * @author zhiyuan
 * @date 2021/4/17 23:51
 */
@Data
public class Config {
    private String name;
    private String value;
    private String remarks;
}
