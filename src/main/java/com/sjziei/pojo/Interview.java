package com.sjziei.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author zhiyuan
 * @date 2021/4/11 12:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Interview {
    private Integer id;
    private String name;
    private Integer sex;
    /**
     * 部门班级
     */
    private String department_class;

    private Long telephone;
    private Long qq;
    /**
     * 升学方式
     */
    private Integer education;
    /**
     * 爱好特长
     */
    private String hobby;
    /**
     * 实践经历
     */
    private String practice;
    /**
     * 自我评价
     */
    private String evaluation;
    /**
     * 备注
     */
    private String remarks;
    private Integer deleted;
    private Date create_time;
    private Date update_time;


}
