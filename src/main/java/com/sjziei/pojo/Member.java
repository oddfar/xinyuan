package com.sjziei.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author zhiyuan
 * @date 2021/4/11 12:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Member {
    private Integer id;
    private String name;
    /**
     * 部门班级
     */
    private String department_class;
    private Integer sex;
    private Long telephone;
    private Long qq;

    private Integer deleted;
    private Date create_time;
    private Date update_time;

}
