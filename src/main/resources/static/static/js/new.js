$(function () {

    var time = (new Date).getHours();
    var msg = '';
    23 < time || time <= 5 ? msg = "你是夜猫子呀？这么晚还不睡觉，明天起的来嘛？" :
        5 < time && time <= 8 ? msg = "早上好！一日之计在于晨，美好的一天就要开始了！" :
            8 < time && time <= 12 ? msg = "上午好！今天学习怎么样，受益匪浅吧！" :
                12 < time && time <= 14 ? msg = "中午了，学习了一上午，现在是午餐时间！" :
                    14 < time && time <= 17 ? msg = "午后很容易犯困呢，今天的运动目标完成了吗？" :
                        17 < time && time <= 19 ? msg = "傍晚了！窗外夕阳的景色很美丽呢，最美不过夕阳红~" :
                            19 < time && time <= 21 ? msg = "晚上好，今天过得怎么样？" :
                                21 < time && time <= 23 && (msg = "已经这么晚了呀，早点休息吧，晚安~");
    layer.msg("Hi~ 欢迎访问本站！" + '<br/>' + msg);

});

$(function () {
    $("#loading").fadeOut(500);
});